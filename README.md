##Z-Score

Um Z-Score de uma observação é o número de desvios padrão acima ou abaixo da média da população. Para calcular um Z-Score, é necessário saber a média e o desvio padrão. O Z-Score é um método muito utilizado na identificação de outliers e de implementação muito variada.