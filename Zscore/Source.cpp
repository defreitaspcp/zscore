/*
Author: de Freitas, P.C.P
Description - ZScore
*/
#include<iostream>
#include<cmath>
using namespace std;
double mean(double data[], int length)
{
	double sum = 0.0;
	for (size_t i = 0; i < length; i++)
	{
		sum += data[i];
	}
	return sum / length;
}
double variance(double data[], int length)
{
	double sum = 0.0;
	for (size_t i = 0; i < length; i++)
	{
		sum += pow(data[i] - mean(data, length), 2);
	}
	return sum / (length-1);
}
double standardDeviation(double data[], int length)
{
	return sqrt(variance(data,length));
}
double zScore(double data[], int length, double value)
{
	/*
	Na f�rmula, "value" representa o valor da amostra que voc� deseja examinar.
	*/
	double a = value - mean(data, length);
	double b = standardDeviation(data, length);
	return a / b;
}
int main()
{
	double teste[5] = { 7,8,8,7.5,9 };
	cout << "Mean : " << mean(teste, 5) << endl;
	cout << "Variance : " << variance(teste, 5) << endl;
	cout << "Standard Deviation : " << standardDeviation(teste, 5) << endl;
	cout << "ZScore : " << zScore(teste,5,7.5) << endl;

	return 0;
}